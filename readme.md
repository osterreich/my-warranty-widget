# My Warranty Widget

To run project locally:

- Install node modules by running: `npm i`
- Launch dev server: `npm run dev`
- Follow [localhost:3000](http://localhost:3005) to open project in Browser

© 2022 Nikita Filipenia
