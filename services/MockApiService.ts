import mockProducts from '../assets/mock-products-response.json';
import { IProductBundle } from '../interfaces';

class MockApiService {
  constructor() {}

  async enroll(...params: any[]): Promise<any> {
    console.log('Enroll params >', params);

    return new Promise((resolve) =>
      setTimeout(() => resolve({ data: { statusCode: 200 } }), 2500)
    );
  }

  async getProducts(...params: any[]): Promise<IProductBundle[]> {
    console.log('Get products params >', params);

    return new Promise((resolve) =>
      setTimeout(() => resolve(mockProducts), 500)
    );
  }
}

export default new MockApiService();
