import { LegacyRef, InputHTMLAttributes, forwardRef } from 'react';
import { ErrorOption } from 'react-hook-form';
import classNames from 'classnames';

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  error?: ErrorOption;
  submitted?: boolean;
}

const Input = forwardRef(
  ({ error, submitted, ...inputProps }: InputProps, ref) => {
    return (
      <div>
        <input
          {...inputProps}
          ref={ref as LegacyRef<HTMLInputElement>}
          className={classNames(
            'form-control',
            submitted && (error ? 'is-invalid' : 'is-valid')
          )}
        />
        {submitted && error && (
          <div className='invalid-feedback d-block m-0'>
            {error.message}&nbsp;
          </div>
        )}
      </div>
    );
  }
);

Input.displayName = 'Input';

export default Input;
