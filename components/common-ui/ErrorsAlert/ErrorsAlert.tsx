import 'react';

interface IErrorsAlertProps {
  errors: string[];
}

const ErrorsAlert = ({ errors }: IErrorsAlertProps) => {
  const isSingleError = errors.length === 1;

  return errors.length ? (
    <div className='alert alert-danger mb-5' role='alert'>
      {isSingleError ? (
        errors[0]
      ) : (
        <ul className='m-0'>
          {errors.map((errorMessage, index) => (
            <li key={index}>{errorMessage}</li>
          ))}
        </ul>
      )}
    </div>
  ) : null;
};

export default ErrorsAlert;
