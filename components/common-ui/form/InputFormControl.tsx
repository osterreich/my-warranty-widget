import {
  useController,
  FieldValues,
  FieldError,
  useFormState,
} from 'react-hook-form';

interface FieldProps extends FieldValues {
  error?: FieldError;
}

export interface InputFormControlProps {
  name: string;
  children: (fieldProps: FieldProps) => JSX.Element;
}

const InputFormControl = ({ name, children }: InputFormControlProps) => {
  const {
    field,
    fieldState: { error },
  } = useController({ name });

  const { isSubmitted: submitted } = useFormState();

  return children({ ...field, error, submitted });
};

export default InputFormControl;
