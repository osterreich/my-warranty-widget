import { forwardRef, useMemo } from 'react';
import InputMask, { Props as InputMaskProps } from 'react-input-mask';
import Input, { InputProps } from './Input';
import { getCommonMaskProps, CommonMaskValue } from '../../utils/masks';

export interface InputMaskControllerProps extends InputProps {
  mask: CommonMaskValue;
  inputMaskProps?: Omit<InputMaskProps, 'mask'> & { maskChar?: string };
}

const InputMaskController = forwardRef(
  ({ mask, inputMaskProps, ...inputProps }: InputMaskControllerProps, ref) => {
    const maskProps = useMemo(
      () =>
        mask
          ? Object.assign({}, inputMaskProps, getCommonMaskProps(mask))
          : undefined,
      [mask, inputMaskProps]
    );

    if (maskProps) {
      const {
        onChange,
        onMouseDown,
        onFocus,
        onBlur,
        value,
        disabled,
        readOnly,
        ...otherInputProps
      } = inputProps;
      return (
        <InputMask
          {...maskProps}
          {...{
            onChange,
            onMouseDown,
            onFocus,
            onBlur,
            value,
            disabled,
            readOnly,
          }}
        >
          {() => <Input ref={ref} {...otherInputProps} disabled={disabled} />}
        </InputMask>
      );
    }
    return <Input ref={ref} {...inputProps} />;
  }
);

InputMaskController.displayName = 'InputMaskController';

export default InputMaskController;
