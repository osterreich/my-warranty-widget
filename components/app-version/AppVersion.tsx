import classNames from 'classnames';

import styles from './AppVersion.module.scss';

export default function AppVersion() {
  return (
    <div
      className={classNames(styles.version, 'text-center text-muted no-select')}
    >
      {process.env.APP_VERSION}
    </div>
  );
}
