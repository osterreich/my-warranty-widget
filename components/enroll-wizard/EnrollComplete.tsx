import _ from 'lodash';

import { useAppSelector } from '../../store';
import { useDispatch } from 'react-redux';
import { enrollActions } from '../../store/enroll';

const EnrollComplete = () => {
  const dispatch = useDispatch();

  const { selectedProduct, duration } = useAppSelector(
    (state) => state.products
  );

  return (
    <div className='text-center'>
      <h1 className='mb-5 text-primary'>Your payment was successful! </h1>
      <p>
        You have purchased the Home warranty plan{' '}
        <span className='text-primary fw-bold'>{selectedProduct?.name}</span>{' '}
        for {duration === 1 ? 'one month' : 'one year'}.
        <br />
        Thank you for your purchase. Please check your email for important
        information regarding your policy and creating an online portal account.
        <br />
        <br />
        We look forward to serving you.
      </p>
      <button
        className='btn btn-primary mt-2'
        onClick={() => dispatch(enrollActions.reset())}
      >
        Enroll now
      </button>
    </div>
  );
};

export default EnrollComplete;
