import { ChangeEvent } from 'react';
import _ from 'lodash';
import classNames from 'classnames';

import { useAppDispatch, useAppSelector } from '../../store';
import { productsActions } from '../../store/products';

const ProductBundleSelect = () => {
  const dispatch = useAppDispatch();
  const { products, selectedProduct, duration } = useAppSelector(
    (state) => state.products
  );

  const onInputChange = (e: ChangeEvent<HTMLSelectElement>) => {
    const { value } = e.target;
    const product = products.find((i) => i.productID === value);
    product && dispatch(productsActions.setSelectedProduct(product));
  };

  if (!selectedProduct) return null;

  return (
    <div className='col'>
      <select
        value={selectedProduct?.productID}
        onChange={onInputChange}
        className={'form-select form-select-lg mb-3'}
      >
        {products.map((product) => (
          <option value={product.productID} key={product.productID}>
            {/* TODO Remove category display */}
            {product.name} ({product.category})
          </option>
        ))}
      </select>
      {/* TODO Remove hardcoded value*/}
      <h5 className='card-title'>Coverage Limit: $15000.00</h5>
      <div>{selectedProduct.description}</div>

      <div
        className='btn-group my-3'
        role='group'
        aria-label='Basic outlined example'
      >
        <button
          type='button'
          onClick={() => dispatch(productsActions.setDuration(1))}
          className={classNames('btn btn-outline-primary', {
            active: duration === 1,
          })}
        >
          Pay Monthly
        </button>
        <button
          type='button'
          onClick={() => dispatch(productsActions.setDuration(12))}
          className={classNames('btn btn-outline-primary', {
            active: duration === 12,
          })}
        >
          Pay Annually
        </button>
      </div>

      <h2 className='card-title'>Your Quote:</h2>
      <h1 className='card-title display-3 text-break'>
        ${(selectedProduct.price * duration).toFixed(2)}
        <span className='display-6'>{duration === 12 ? '/yr' : '/mo'}</span>
      </h1>
    </div>
  );
};

export default ProductBundleSelect;
