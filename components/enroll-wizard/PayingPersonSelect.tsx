import _ from 'lodash';
import classNames from 'classnames';

import { useAppDispatch, useAppSelector } from '../../store';
import { enrollActions } from '../../store/enroll';

const PayingPersonSelect = () => {
  const dispatch = useAppDispatch();
  const { isPayForMyself } = useAppSelector((state) => state.enroll);

  return (
    <div className='col'>
      <h5>Paying for this plan is:</h5>
      <div
        className='btn-group my-2'
        role='group'
        aria-label='Basic outlined example'
      >
        <button
          type='button'
          onClick={() => dispatch(enrollActions.setPayForMyselfState(true))}
          className={classNames('btn btn-outline-primary', {
            active: isPayForMyself,
          })}
        >
          Myself
        </button>
        <button
          type='button'
          onClick={() => dispatch(enrollActions.setPayForMyselfState(false))}
          className={classNames('btn btn-outline-primary', {
            active: !isPayForMyself,
          })}
        >
          Someone else
        </button>
      </div>
    </div>
  );
};

export default PayingPersonSelect;
