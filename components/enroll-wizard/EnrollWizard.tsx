import { useEffect } from 'react';

import { useAppSelector } from '../../store';
import EnrollCustomerInfo from './EnrollCustomerInfo';
import EnrollBillingInfo from './EnrollBillingInfo';
import EnrollConfirm from './EnrollConfirm';
import EnrollProgressbar from './EnrollProgressbar';
import EnrollComplete from './EnrollComplete';

const EnrollWizard = () => {
  const { slide } = useAppSelector((state) => state.enroll);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [slide]);

  const slideElement = () => {
    switch (slide) {
      case 0:
        return <EnrollCustomerInfo />;
      case 1:
        return <EnrollBillingInfo />;
      case 2:
        return <EnrollConfirm />;
      case 3:
        return <EnrollComplete />;
      default:
        return null;
    }
  };

  return (
    <div className='p-3 p-md-5 m-0'>
      <EnrollProgressbar />
      {slideElement()}
    </div>
  );
};

export default EnrollWizard;
