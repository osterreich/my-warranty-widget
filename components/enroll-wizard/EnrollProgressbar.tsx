import { PostMessageEvent } from '../../enums';
import { useAppSelector } from '../../store';

const EnrollProgressbar = () => {
  const { slide } = useAppSelector((state) => state.enroll);

  const close = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    window.parent.postMessage({ action: PostMessageEvent.CloseWindow }, '*');
  };

  return (
    <div className='pb-3 pb-sm-5 d-flex justify-content-between'>
      <div className='w-50'>
        <h1>Step {slide + 1}</h1>
        <div className='progress w-75' style={{ height: 2 }}>
          <div
            className='progress-bar'
            role='progressbar'
            style={{ width: (slide / 2) * 100 + '%' }}
            aria-valuenow={0}
            aria-valuemin={0}
            aria-valuemax={100}
          ></div>
        </div>
      </div>
      <div>
        <button className='btn btn-lg' onClick={close}>
          <i className='text-primary fa fa-lg fa-xmark'></i>
        </button>
      </div>
    </div>
  );
};

export default EnrollProgressbar;
