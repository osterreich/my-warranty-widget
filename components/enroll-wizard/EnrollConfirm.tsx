import _ from 'lodash';
import axios from 'axios';
import classNames from 'classnames';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';

import styles from './EnrollWizard.module.scss';
import { useAppSelector } from '../../store';
import EnrollCheckoutDetails from './EnrollCheckoutDetails';
import { useDispatch } from 'react-redux';
import { enrollActions } from '../../store/enroll';
import { hideCardNumber } from '../../utils';
import EnrollNavigation from './EnrollNavigation';
import { IEnrollRequestBody, IEnrollResponse } from '../../interfaces/enroll';
import ErrorsAlert from '../common-ui/ErrorsAlert/ErrorsAlert';

const EnrollConfirm = () => {
  const router = useRouter();
  const dispatch = useDispatch();

  const [isPending, setIsPending] = useState(false);
  const [isCompleted, setIsCompleted] = useState(false);
  const [submitErrors, setSubmitErrors] = useState<string[]>([]);

  const {
    customerInfo,
    billingInfo,
    paymentInfo,
    isTermsConfirmed,
    isPayForMyself,
  } = useAppSelector((state) => state.enroll);

  const { selectedProduct } = useAppSelector((state) => state.products);

  const { partnerSecret, partnerSalesCode } = useAppSelector(
    (state) => state.app
  );

  useEffect(() => {
    if (isCompleted) {
      dispatch(enrollActions.setSlide(3));
    }
  }, [dispatch, router, isCompleted]);

  const submit = async () => {
    try {
      window.scrollTo(0, 0);
      setIsPending(true);

      const body: IEnrollRequestBody = {
        customer: customerInfo,
        billing: billingInfo,
        payment: paymentInfo,
        product: {
          productID: selectedProduct!.productID,
        },
        partner: {
          secretKey: partnerSecret,
          salesCode: partnerSalesCode,
        },
        options: {
          isPayForMyself,
          isTermsConfirmed,
        },
      };

      const res = await axios.post('api/enroll', body);
      const enrollResponse: IEnrollResponse = res.data;

      if (enrollResponse) {
        setIsCompleted(true);
      }
    } catch (error: any) {
      const errorMessage = axios.isAxiosError(error)
        ? error.response?.data.message
        : error.message;
      setSubmitErrors([errorMessage]);
    }
    setIsPending(false);
  };

  const setSlide = (slide: number) => {
    dispatch(enrollActions.setSlide(slide));
  };

  return (
    <div className='row'>
      <ErrorsAlert errors={submitErrors} />
      {isPending && <h1 className='mb-5'>Processing your payment... </h1>}
      <div
        className={classNames(
          'col-12 col-sm-5 border-end pe-md-5',
          styles.input_column
        )}
      >
        <div className='row'>
          <div className='col-12 border-bottom mb-3'>
            <h5>
              Customer Information
              <i
                className='link-primary fa fa-pen-square ms-1'
                onClick={() => setSlide(0)}
              />
            </h5>
            <div className=''>
              {customerInfo.firstName} {customerInfo.lastName}
              <br />
              {customerInfo.email}
              <br />
              {customerInfo.phone}
              <br />
              {customerInfo.address}
              <br />
              {customerInfo.city}, {customerInfo.state} {customerInfo.zip}
            </div>
            <p className='badge bg-primary mt-2'>
              Paying for this plan is{' '}
              {isPayForMyself ? 'myself' : 'someone else'}
            </p>
          </div>
          <div className='col-12 border-bottom mb-3 pb-3'>
            <h5>
              Billing Information
              <i
                className='link-primary fa fa-pen-square ms-1'
                onClick={() => setSlide(1)}
              />
            </h5>
            <div>
              {billingInfo.firstName} {billingInfo.lastName}
              <br />
              {billingInfo.email}
              <br />
              {billingInfo.phone}
              <br />
              {billingInfo.address}
              <br />
              {billingInfo.city}, {billingInfo.state} {billingInfo.zip}
            </div>
          </div>
          <div className='col-12'>
            <h5>
              Payment Information
              <i
                className='link-primary fa fa-pen-square ms-1'
                onClick={() => setSlide(1)}
              />
            </h5>
            <div>
              {hideCardNumber(paymentInfo.cardNumber)}
              <br />
              {paymentInfo.expDate}
            </div>
          </div>
        </div>
      </div>
      <div className='col pt-4 pt-sm-0 ps-md-5'>
        <h1 className='mb-4'>Checkout Details:</h1>
        <EnrollCheckoutDetails />
      </div>
      <EnrollNavigation>
        <button
          className='btn btn-lg btn-info ms-2 px-4'
          onClick={submit}
          disabled={!isTermsConfirmed || isPending || isCompleted}
        >
          Confirm purchase
        </button>
      </EnrollNavigation>
    </div>
  );
};

export default EnrollConfirm;
