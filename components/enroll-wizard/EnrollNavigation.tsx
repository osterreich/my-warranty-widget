import classNames from 'classnames';
import { ReactChild } from 'react';

import { useAppSelector } from '../../store';
import { useDispatch } from 'react-redux';
import { enrollActions } from '../../store/enroll';

interface PropsType {
  nextButton?: boolean;
  backButton?: boolean;
  resetButton?: boolean;
  children?: ReactChild | ReactChild[];
}

const EnrollNavigation = ({
  children,
  nextButton,
  backButton,
  resetButton,
}: PropsType) => {
  const { slide } = useAppSelector((state) => state.enroll);
  const dispatch = useDispatch();

  const goBack = () => dispatch(enrollActions.setSlide(Math.max(0, slide - 1)));
  const goNext = () => dispatch(enrollActions.setSlide(Math.min(3, slide + 1)));
  const reset = () => dispatch(enrollActions.reset());

  return (
    <div className={classNames('d-flex justify-content-between mt-5')}>
      <div>
        {resetButton && (
          <button
            className='btn btn-lg btn-outline-danger border-white'
            onClick={reset}
          >
            Reset
          </button>
        )}
      </div>
      <div>
        {backButton && (
          <button
            type='button'
            className='btn btn-lg btn-primary px-4 ms-2'
            onClick={goBack}
          >
            Back
          </button>
        )}
        {nextButton && (
          <button className='btn btn-lg btn-primary ms-2 px-4' onClick={goNext}>
            Next
          </button>
        )}
        {children}
      </div>
    </div>
  );
};

export default EnrollNavigation;
