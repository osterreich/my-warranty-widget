import { ChangeEvent, useEffect, useState } from 'react';
import _ from 'lodash';
import classNames from 'classnames';

import styles from './EnrollWizard.module.scss';
import { TAX_RATE } from '../../config';
import { ProductBundleCategory } from '../../enums';
import { useAppDispatch, useAppSelector } from '../../store';
import { enrollActions } from '../../store/enroll';
import disclosureHome from '../../public/disclosures/home.html';
import disclosureHomeService from '../../public/disclosures/home-service.html';
import disclosureElectronics from '../../public/disclosures/electronics.html';
import disclosureAllProducts from '../../public/disclosures/all-products.html';

const disclosures = {
  [ProductBundleCategory.HomeWarranty]: disclosureHome,
  [ProductBundleCategory.ApplianceWarranty]: disclosureHome,
  [ProductBundleCategory.HomeService]: disclosureHomeService,
  [ProductBundleCategory.ElectronicsWarranty]: disclosureElectronics,
};

const EnrollCheckoutDetails = () => {
  const dispatch = useAppDispatch();

  const { selectedProduct, duration } = useAppSelector(
    (state) => state.products
  );
  const { customerInfo, isTermsConfirmed, isPayForMyself } = useAppSelector(
    (state) => state.enroll
  );

  const [totalPrice, setTotalPrice] = useState(0);
  const [taxPrice, setTaxPrice] = useState(0);
  const [disclosure, setDisclosure] = useState('');

  const periodLabels = {
    1: 'Monthly',
    12: 'Annually',
  };

  useEffect(() => {
    if (selectedProduct) {
      console.log(selectedProduct.category);

      const disclosure = isPayForMyself
        ? disclosures[selectedProduct.category as keyof typeof disclosures]
        : disclosureAllProducts;

      disclosure &&
        setDisclosure(disclosure.replace('{{email}}', customerInfo.email));
    }
  }, [selectedProduct, isPayForMyself, customerInfo]);

  useEffect(() => {
    if (selectedProduct) {
      const productPrice = selectedProduct.price * duration;
      const taxPrice = productPrice * TAX_RATE;
      setTotalPrice(productPrice + taxPrice);
      setTaxPrice(taxPrice);
    }
  }, [selectedProduct, duration]);

  const toggleTermsConfirmed = (e: ChangeEvent<HTMLInputElement>) => {
    const { checked } = e.target;
    dispatch(enrollActions.setTermsConfirmedState(checked));
  };

  if (!selectedProduct) return null;

  return (
    <>
      <div className='rounded  mb-4  text'>
        <div className='col'>
          <div className='d-flex justify-content-between align-items-center border-bottom mb-3 pb-3'>
            <div>Product</div>
            <div className='text-end'>
              {selectedProduct.name}
              <br />
              {/* TODO Remove hardcoded value*/}
              <small>$15000.00</small>
            </div>
          </div>
          <div className='d-flex justify-content-between align-items-center border-bottom mb-3 pb-3'>
            <div>Price/Frequency</div>
            <div className='text-end'>
              ${(selectedProduct.price * duration).toFixed(2)} <br />
              <small>
                {
                  //@ts-ignore
                  periodLabels[duration.toString()]
                }
              </small>
            </div>
          </div>
          <div className='d-flex justify-content-between align-items-center border-bottom mb-3 pb-3'>
            <div>Tax</div>
            <div>${taxPrice.toFixed(2)}</div>
          </div>
          <div className='d-flex justify-content-between align-items-center'>
            <h3 className='text-primary m-0'>Total</h3>
            <div className='display-2 text-primary'>
              ${totalPrice.toFixed(2)}
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className='col form-check'>
          <label className='form-check-label'>
            <input
              type={'checkbox'}
              className='form-check-input'
              checked={isTermsConfirmed}
              onChange={toggleTermsConfirmed}
            />
            I confirm the following:
          </label>
        </div>
        <ol className={classNames(styles.confirm_list)}>
          <div dangerouslySetInnerHTML={{ __html: disclosure }} />
        </ol>
      </div>
    </>
  );
};

export default EnrollCheckoutDetails;
