import { ChangeEvent, useEffect } from 'react';
import _ from 'lodash';
import classNames from 'classnames';
import { yupResolver } from '@hookform/resolvers/yup';
import { SubmitHandler, useForm, FormProvider } from 'react-hook-form';

import styles from './EnrollWizard.module.scss';
import usStates from '../../assets/us-states.json';
import { useAppDispatch, useAppSelector } from '../../store';
import { enrollActions } from '../../store/enroll';
import EnrollNavigation from './EnrollNavigation';
import { billingInfoSchema } from '../../utils/validators';
import { InputFormControl, Input, InputMask } from '../common-ui';
import { IEnrollBillingInfo } from '../../interfaces/enroll';

const rowClassNames = 'row g-1 my-1';

const EnrollBillingInfo = () => {
  const dispatch = useAppDispatch();
  const { billingInfo, customerInfo, paymentInfo, isUseCustomerInfo, slide } =
    useAppSelector((state) => state.enroll);
  const formContext = useForm<IEnrollBillingInfo>({
    mode: 'onBlur',
    resolver: yupResolver(billingInfoSchema),
    defaultValues: {
      ...billingInfo,
      ...paymentInfo,
      state: billingInfo.state || 'State',
    },
  });

  const {
    formState: { errors, isSubmitted },
    handleSubmit,
    register,
    setValue,
  } = formContext;

  useEffect(() => {
    if (isUseCustomerInfo) {
      _.map(customerInfo, (value, key) => {
        setValue(key as keyof IEnrollBillingInfo, value, {
          shouldValidate: true,
        });
      });
    }
  }, [isUseCustomerInfo, customerInfo, setValue]);

  const toggleUseCustomerInfo = (e: ChangeEvent<HTMLInputElement>) => {
    const { checked } = e.target;
    dispatch(enrollActions.setUseCustomerInfoState(checked));
  };

  const onSubmit: SubmitHandler<IEnrollBillingInfo> = (data) => {
    const { cardNumber, expDate, csvNumber, ...billingInfo } = data;
    const paymentInfo = { cardNumber, expDate, csvNumber };

    dispatch(enrollActions.setBillingInfo(billingInfo));
    dispatch(enrollActions.setPaymentInfo(paymentInfo));
    dispatch(enrollActions.setSlide(slide + 1));
  };

  const selectElement = (key: keyof IEnrollBillingInfo, options: string[]) => {
    const error = errors[key]?.message;
    const disabled = isUseCustomerInfo && _.keys(customerInfo).includes(key);
    const placeholder = _.startCase(key);

    return (
      <div>
        <select
          {...register(key)}
          disabled={disabled}
          className={classNames(
            isSubmitted && (error ? 'is-invalid' : 'is-valid'),
            'form-select'
          )}
        >
          {[placeholder, ...options].map((option, index) => (
            <option disabled={!index} key={option} value={option}>
              {option}
            </option>
          ))}
        </select>
        {isSubmitted && (
          <div className='invalid-feedback d-block m-0'>{error}&nbsp;</div>
        )}
      </div>
    );
  };

  return (
    <FormProvider {...formContext}>
      <form onSubmit={handleSubmit(onSubmit)} className='row'>
        <div
          className={classNames(
            'col-12 col-sm-6 border-end pe-md-5',
            styles.input_column
          )}
        >
          <h1 className='pb-3'>Billing Information</h1>
          <div className={rowClassNames}>
            <div className='col'>
              <InputFormControl name='firstName'>
                {(field) => (
                  <Input
                    {...field}
                    placeholder='First Name'
                    disabled={isUseCustomerInfo}
                  />
                )}
              </InputFormControl>
            </div>
            <div className='col'>
              <InputFormControl name='lastName'>
                {(field) => (
                  <Input
                    {...field}
                    placeholder='Last Name'
                    disabled={isUseCustomerInfo}
                  />
                )}
              </InputFormControl>
            </div>
          </div>
          <div className={rowClassNames}>
            <div className='col-12 col-sm-6'>
              <InputFormControl name='email'>
                {(field) => (
                  <Input
                    {...field}
                    placeholder='Email'
                    disabled={isUseCustomerInfo}
                  />
                )}
              </InputFormControl>
            </div>
            <div className='col-12 col-sm-6'>
              <InputFormControl name='emailConfirm'>
                {(field) => (
                  <Input
                    {...field}
                    placeholder='Email Confirm'
                    disabled={isUseCustomerInfo}
                  />
                )}
              </InputFormControl>
            </div>
          </div>
          <div className={rowClassNames}>
            <div className='col-12 col-sm-6'>
              <InputFormControl name='phone'>
                {(field) => (
                  <InputMask
                    {...field}
                    mask='phone'
                    type='tel'
                    placeholder='Phone'
                    disabled={isUseCustomerInfo}
                  />
                )}
              </InputFormControl>
            </div>
            <div className='col-12 col-sm-6'>
              <InputFormControl name='address'>
                {(field) => (
                  <Input
                    {...field}
                    placeholder='Address'
                    disabled={isUseCustomerInfo}
                  />
                )}
              </InputFormControl>
            </div>
          </div>
          <div className={rowClassNames}>
            <div className='col-12 col-sm-6'>
              <InputFormControl name='city'>
                {(field) => (
                  <Input
                    {...field}
                    placeholder='City'
                    disabled={isUseCustomerInfo}
                  />
                )}
              </InputFormControl>
            </div>
            <div className='col'>
              {selectElement('state', Object.keys(usStates))}
            </div>
            <div className='col'>
              <InputFormControl name='zip'>
                {(field) => (
                  <InputMask
                    {...field}
                    mask='zip'
                    placeholder='Zip'
                    disabled={isUseCustomerInfo}
                  />
                )}
              </InputFormControl>
            </div>
          </div>
          <div className={rowClassNames}>
            <div className='col form-check'>
              <label className='form-check-label'>
                <input
                  type={'checkbox'}
                  className='form-check-input'
                  checked={isUseCustomerInfo}
                  onChange={toggleUseCustomerInfo}
                />
                Same as customer information
              </label>
            </div>
          </div>
        </div>
        <div className='col pt-4 pt-sm-0 ps-md-5'>
          <h1 className='pb-3'>Payment Method</h1>
          <div className={rowClassNames}>
            <div className='col'>
              <InputFormControl name='cardNumber'>
                {(field) => {
                  const mask =
                    field.value[0] === '3'
                      ? 'cardNumberAmericanExpress'
                      : 'cardNumber';
                  return (
                    <InputMask
                      {...field}
                      mask={mask}
                      placeholder='Card Number'
                      autoComplete='cc-number'
                    />
                  );
                }}
              </InputFormControl>
            </div>
            <div id='emailHelp' className='form-text'>
              Please enter your credit card number
            </div>
          </div>
          <div className={rowClassNames}>
            <div className='col'>
              <InputFormControl name='expDate'>
                {(field) => (
                  <InputMask
                    {...field}
                    mask='expDate'
                    placeholder='MM/YYYY'
                    autoComplete='cc-exp'
                  />
                )}
              </InputFormControl>
            </div>
            <div className='col'>
              <InputFormControl name='csvNumber'>
                {(field) => (
                  <InputMask
                    {...field}
                    mask='cvv'
                    type='password'
                    placeholder='CVV'
                    autoComplete='cc-csc'
                  />
                )}
              </InputFormControl>
            </div>
          </div>
        </div>
        <EnrollNavigation backButton>
          <input
            type='submit'
            className='btn btn-lg btn-primary ms-2 px-4'
            value={'Next'}
          />
        </EnrollNavigation>
      </form>
    </FormProvider>
  );
};

export default EnrollBillingInfo;
