import _ from 'lodash';
import { useState } from 'react';
import classNames from 'classnames';
import { yupResolver } from '@hookform/resolvers/yup';
import { SubmitHandler, useForm, FormProvider } from 'react-hook-form';

import styles from './EnrollWizard.module.scss';
import usStates from '../../assets/us-states.json';
import limitations from '../../assets/limitations.json';
import { useAppDispatch, useAppSelector } from '../../store';
import { enrollActions } from '../../store/enroll';
import { IEnrollUserInfo } from '../../interfaces/enroll';
import { customerInfoSchema } from '../../utils/validators';
import PayingPersonSelect from './PayingPersonSelect';
import ProductBundleSelect from './ProductBundleSelect';
import EnrollNavigation from './EnrollNavigation';
import { InputFormControl, Input, InputMask } from '../common-ui';
import ErrorsAlert from '../common-ui/ErrorsAlert/ErrorsAlert';

const rowClassNames = 'row g-1 my-1';
const serviceLimitErrorMessage =
  'Unfortunately, we are unable to accept orders in your state at this time. Please return in the future to enroll in coverage from My Warranty.';
const userExistErrorMessage =
  'This user already exists, please log in to proceed.';

const EnrollCustomerInfo = () => {
  const dispatch = useAppDispatch();

  const { customerInfo, slide } = useAppSelector((state) => state.enroll);
  const { selectedProduct } = useAppSelector((state) => state.products);

  const [submitErrors, setSubmitErrors] = useState<string[]>([]);

  const formContext = useForm<IEnrollUserInfo>({
    mode: 'onBlur',
    resolver: yupResolver(customerInfoSchema),
    defaultValues: { ...customerInfo, state: customerInfo.state || 'State' },
  });

  const {
    formState: { errors, isSubmitted },
    handleSubmit,
    register,
  } = formContext;

  const onSubmit: SubmitHandler<IEnrollUserInfo> = async (data) => {
    const errors = [];

    !isServiceAvailable(data) && errors.push(serviceLimitErrorMessage);
    !(await isNewUser(data)) && errors.push(userExistErrorMessage);

    if (errors.length) {
      window.scrollTo(0, 0);
      setSubmitErrors(errors);
    } else {
      dispatch(enrollActions.setCustomerInfo(data));
      dispatch(enrollActions.setSlide(slide + 1));
    }
  };

  const isServiceAvailable = (data: IEnrollUserInfo) => {
    if (selectedProduct) {
      const { state } = data;
      const { category } = selectedProduct;

      return !limitations.find(
        (item) => item.category === category && item.states.includes(state)
      );
    }
  };

  const isNewUser = async (data: IEnrollUserInfo) => {
    // TODO
    const { email } = data;
    return true;
  };

  const selectElement = (key: keyof IEnrollUserInfo, options: string[]) => {
    const error = errors[key]?.message;
    const placeholder = _.startCase(key);

    return (
      <div>
        <select
          {...register(key)}
          className={classNames(
            isSubmitted && (error ? 'is-invalid' : 'is-valid'),
            'form-select'
          )}
        >
          {[placeholder, ...options].map((option, index) => (
            <option disabled={!index} key={option} value={option}>
              {option}
            </option>
          ))}
        </select>
        {isSubmitted && (
          <div className='invalid-feedback d-block m-0'>{error}&nbsp;</div>
        )}
      </div>
    );
  };

  return (
    <FormProvider {...formContext}>
      <form onSubmit={handleSubmit(onSubmit)} className='row'>
        <ErrorsAlert errors={submitErrors} />
        <div
          className={classNames(
            'col-12 col-sm-6 border-end pe-md-5',
            styles.input_column
          )}
        >
          <h1 className='pb-3'>Customer Information</h1>
          <div className={rowClassNames}>
            <div className='col'>
              <InputFormControl name='firstName'>
                {(field) => <Input {...field} placeholder='First Name' />}
              </InputFormControl>
            </div>
            <div className='col'>
              <InputFormControl name='lastName'>
                {(field) => <Input {...field} placeholder='Last Name' />}
              </InputFormControl>
            </div>
          </div>
          <div className={rowClassNames}>
            <div className='col-12 col-sm-6'>
              <InputFormControl name='email'>
                {(field) => (
                  <Input {...field} type='email' placeholder='Email' />
                )}
              </InputFormControl>
            </div>
            <div className='col-12 col-sm-6'>
              <InputFormControl name='emailConfirm'>
                {(field) => <Input {...field} placeholder='Email Confirm' />}
              </InputFormControl>
            </div>
          </div>
          <div className={rowClassNames}>
            <div className='col-12 col-sm-6'>
              <InputFormControl name='phone'>
                {(field) => (
                  <InputMask
                    {...field}
                    mask='phone'
                    type='tel'
                    placeholder='Phone'
                  />
                )}
              </InputFormControl>
            </div>
            <div className='col-12 col-sm-6'>
              <InputFormControl name='address'>
                {(field) => <Input {...field} placeholder='Address' />}
              </InputFormControl>
            </div>
          </div>
          <div className={rowClassNames}>
            <div className='col-12 col-sm-6'>
              <InputFormControl name='city'>
                {(field) => <Input {...field} placeholder='City' />}
              </InputFormControl>
            </div>
            <div className='col'>
              {selectElement('state', Object.keys(usStates))}
            </div>
            <div className='col'>
              <InputFormControl name='zip'>
                {(field) => (
                  <InputMask {...field} mask='zip' placeholder='Zip' />
                )}
              </InputFormControl>
            </div>
          </div>
          <div className={rowClassNames}>
            <div className='col'>
              <PayingPersonSelect />
            </div>
          </div>
        </div>
        <div className='col pt-4 pt-sm-0 ps-md-5'>
          <h1 className='pb-3'>Plan Details</h1>
          <div className={rowClassNames}>
            <ProductBundleSelect />
          </div>
        </div>
        <EnrollNavigation>
          <input
            type='submit'
            className='btn btn-lg btn-primary ms-2 px-4'
            value={'Next'}
          />
        </EnrollNavigation>
      </form>
    </FormProvider>
  );
};

export default EnrollCustomerInfo;
