export interface IProductBundle {
  productID: string;
  name: string;
  type: string;
  category: string;
  description: string;
  price: number;
  coverageLimit: number;
}

export interface IErrorResponse {
  message: string;
}
