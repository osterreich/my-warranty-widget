import { NextApiRequest } from 'next';

export interface IEnrollUserInfo {
  firstName: string;
  lastName: string;
  email: string;
  emailConfirm: string;
  phone: string;
  address: string;
  city: string;
  state: string;
  zip: string;
}

export interface IEnrollPaymentInfo {
  cardNumber: string;
  expDate: string;
  csvNumber: string;
}

export interface IEnrollBillingInfo
  extends IEnrollUserInfo,
    IEnrollPaymentInfo {}

export interface IEnrollRequestBody {
  customer: IEnrollUserInfo;
  billing: IEnrollUserInfo;
  payment: IEnrollPaymentInfo;
  partner: {
    secretKey: string;
    salesCode: string;
  };
  product: {
    productID: string;
  };
  options: {
    isPayForMyself: boolean;
    isTermsConfirmed: boolean;
  };
}

export interface IEnrollRequest extends NextApiRequest {
  body: IEnrollRequestBody;
}

export interface IEnrollResponse {
  contractExternalKey: string;
  productID: string;
  billingInformationID: string;
  paymentType: string;
  contractID: string;
  customerID: string;
}
