import * as yup from 'yup';

const errorRequired = 'Required field';
const errorInvalid = 'Invalid value';

const userObjectSchema = {
  firstName: yup.string().max(60).required(errorRequired),
  lastName: yup.string().max(60).required(errorRequired),
  email: yup.string().email().max(60).required(errorRequired),
  emailConfirm: yup
    .string()
    .max(60)
    .required(errorRequired)
    .oneOf([yup.ref('email'), null], 'Emails must match'),
  phone: yup
    .string()
    .matches(/^[^_]+$/, errorInvalid)
    .required(errorRequired),
  address: yup.string().max(60).required(errorRequired),
  city: yup.string().max(60).required(errorRequired),
  state: yup.string().length(2, errorRequired).required(errorRequired),
  zip: yup
    .string()
    .length(5, 'Zip must be exactly 5 characters')
    .required(errorRequired),
};

export const customerInfoSchema = yup
  .object({ ...userObjectSchema })
  .required();

export const billingInfoSchema = yup
  .object({
    ...userObjectSchema,
    cardNumber: yup
      .string()
      .matches(/^[^_]+$/, errorInvalid)
      .required(errorRequired),
    expDate: yup
      .string()
      .matches(/^[^_]+$/, errorInvalid)
      .required(errorRequired),
    csvNumber: yup
      .string()
      .min(3, 'CVV number must be at least 3 characters')
      .max(4)
      .required(errorRequired),
  })
  .required();

export const loginSchema = yup
  .object({
    username: yup.string().email().max(60).required(errorRequired),
    password: yup.string().required(errorRequired),
  })
  .required();

export const resetPasswordSchema = yup
  .object({
    isConfirmationStep: yup.boolean(),
    username: yup.string().email().max(60).required(errorRequired),

    code: yup.string().when('isConfirmationStep', {
      is: true,
      then: yup.string().required(errorRequired),
    }),
    password: yup.string().when('isConfirmationStep', {
      is: true,
      then: yup
        .string()
        .required(errorRequired)
        .matches(
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
          'Must Contain at least 8 Characters, One Uppercase, One Lowercase, One Number and One Special Character'
        ),
    }),
    passwordConfirm: yup.string().when('isConfirmationStep', {
      is: true,
      then: yup
        .string()
        .required(errorRequired)
        .oneOf([yup.ref('password'), null], 'Passwords must match'),
    }),
  })
  .required();

export const setupPasswordSchema = yup
  .object({
    password: yup
      .string()
      .required(errorRequired)
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
        'Must Contain at least 8 Characters, One Uppercase, One Lowercase, One Number and One Special Character'
      ),
    passwordConfirm: yup
      .string()
      .required(errorRequired)
      .oneOf([yup.ref('password'), null], 'Passwords must match'),
  })
  .required();
