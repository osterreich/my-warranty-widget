import { Props as InputMaskProps } from 'react-input-mask';
export type CommonMaskValue =
  | 'phone'
  | 'zip'
  | 'cvv'
  | 'card'
  | 'exp-date'
  | string
  | Array<string | RegExp>;

export const getCommonMaskProps = (
  mask: CommonMaskValue
): InputMaskProps | undefined => {
  switch (mask) {
    case 'phone':
      return {
        mask: '+1 (999) 999-9999',
      };
    case 'zip':
      return {
        mask: '99999',
        maskChar: null,
        beforeMaskedValueChange: function (newState) {
          let { value, selection } = newState;
          let cursorPosition = selection ? selection.start : null;

          if (value.endsWith('-')) {
            if (cursorPosition === value.length) {
              cursorPosition--;
              selection = { start: cursorPosition, end: cursorPosition };
            }
            value = value.slice(0, -1);
          }

          return { value, selection };
        },
      };
    case 'cvv':
      return {
        mask: '9999',
        maskChar: null,
      };
    case 'cardNumber':
      return {
        mask: '9999-9999-9999-9999',
      };
    case 'cardNumberAmericanExpress':
      return {
        mask: '9999-999999-99999',
      };
    case 'expDate':
      return {
        mask: 'mM/yYYY',
        formatChars: {
          m: '[0-1]',
          M: '[0-9]',
          y: '2',
          Y: '[0-9]',
        },
      };
    default:
      return {
        mask,
      };
  }
};
