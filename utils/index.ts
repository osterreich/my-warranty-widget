import axios from 'axios';
import { NextApiResponse } from 'next';
import { IErrorResponse } from '../interfaces';

export function hideCardNumber(card: string) {
  let hideNum = '';

  for (let i = 0; i < card.length; i++) {
    hideNum += i < card.length - 4 ? '*' : card[i];
  }

  return hideNum;
}

export function sendError(res: NextApiResponse, error: any) {
  let code = 500;

  const body: IErrorResponse = {
    message: 'Unknown error',
  };

  if (error instanceof Error) {
    if (isErrnoException(error) && error.code === 'ENOENT') {
      body.message = 'Resource not found';
    } else if (axios.isAxiosError(error)) {
      body.message = error.response?.data?.error || error.message;
    } else {
      body.message = error.message;
    }
  }
  res.status(code).json(body);
}

export function isErrnoException(error: any): error is NodeJS.ErrnoException {
  return Object.prototype.hasOwnProperty.call(error, 'code');
}
