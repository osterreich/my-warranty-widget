import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { IProductBundle } from '../../interfaces';

export interface IProductsState {
  products: IProductBundle[];
  selectedProduct: IProductBundle | null;
  duration: number;
}

const initialState: IProductsState = {
  products: [],
  selectedProduct: null,
  duration: 1,
};

export const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    setProducts: (state, action: PayloadAction<IProductBundle[]>) => {
      state.products = action.payload;
    },
    setDuration: (state, action: PayloadAction<number>) => {
      state.duration = action.payload;
    },
    setSelectedProduct: (state, action: PayloadAction<IProductBundle>) => {
      state.selectedProduct = action.payload;
    },
  },
});

export const { actions: productsActions, reducer: productsReducer } =
  productsSlice;
