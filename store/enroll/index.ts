import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { IEnrollPaymentInfo, IEnrollUserInfo } from '../../interfaces/enroll';

export interface IEnrollState {
  slide: number;
  isTermsConfirmed: boolean;
  isUseCustomerInfo: boolean;
  isPayForMyself: boolean;
  customerInfo: IEnrollUserInfo;
  billingInfo: IEnrollUserInfo;
  paymentInfo: IEnrollPaymentInfo;
}

const initialState: IEnrollState = {
  slide: 0,
  isTermsConfirmed: false,
  isUseCustomerInfo: false,
  isPayForMyself: true,
  customerInfo: {
    firstName: '',
    lastName: '',
    email: '',
    emailConfirm: '',
    phone: '',
    address: '',
    city: '',
    state: '',
    zip: '',
  },
  billingInfo: {
    firstName: '',
    lastName: '',
    email: '',
    emailConfirm: '',
    phone: '',
    address: '',
    city: '',
    state: '',
    zip: '',
  },
  paymentInfo: {
    cardNumber: '',
    expDate: '',
    csvNumber: '',
  },
};

export const enrollSlice = createSlice({
  name: 'enroll',
  initialState,
  reducers: {
    setCustomerInfo: (state, action: PayloadAction<IEnrollUserInfo>) => {
      state.customerInfo = { ...state.customerInfo, ...action.payload };
    },
    setBillingInfo: (state, action: PayloadAction<IEnrollUserInfo>) => {
      state.billingInfo = { ...state.billingInfo, ...action.payload };
    },
    setPaymentInfo: (state, action: PayloadAction<IEnrollPaymentInfo>) => {
      state.paymentInfo = { ...state.paymentInfo, ...action.payload };
    },
    setSlide: (state, action: PayloadAction<number>) => {
      state.slide = action.payload;
    },
    setTermsConfirmedState: (state, action: PayloadAction<boolean>) => {
      state.isTermsConfirmed = action.payload;
    },
    setUseCustomerInfoState: (state, action: PayloadAction<boolean>) => {
      state.isUseCustomerInfo = action.payload;
    },
    setPayForMyselfState: (state, action: PayloadAction<boolean>) => {
      state.isPayForMyself = action.payload;
    },
    reset: () => initialState,
  },
});

export const { actions: enrollActions, reducer: enrollReducer } = enrollSlice;
