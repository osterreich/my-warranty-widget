import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface IAppState {
  partnerSecret: string;
  partnerSalesCode: string;
  visibleModalID: number | null;
}

const initialState: IAppState = {
  partnerSecret: '',
  partnerSalesCode: '',
  visibleModalID: null,
};

export const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setPartnerSecret: (state, action: PayloadAction<string>) => {
      state.partnerSecret = action.payload;
    },
    setPartnerSalesCode: (state, action: PayloadAction<string>) => {
      state.partnerSalesCode = action.payload;
    },
    openModal: (state, action: PayloadAction<number>) => {
      state.visibleModalID = action.payload;
    },
    closeModal: (state) => {
      state.visibleModalID = null;
    },
  },
});

export const { actions: appActions, reducer: appReducer } = appSlice;
