/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,

  webpack: (config, { webpack, buildId, isServer }) => {
    config.module.rules.push({
      test: /\.html$/i,
      loader: 'html-loader',
    });

    config.plugins.push(
      new webpack.DefinePlugin({
        'process.env.APP_VERSION': JSON.stringify(
          require('./package.json').version
        ),
      })
    );
    return config;
  },
};

module.exports = nextConfig;
