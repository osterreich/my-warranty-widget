export enum Modal {
  EnrollContractReview,
  Enroll,
}

export enum AuthState {
  Login,
  ResetPassword,
  SetupPassword,
}

export enum ProductBundleCategory {
  HomeWarranty = 'THW',
  ApplianceWarranty = 'TAW',
  ElectronicsWarranty = 'TEW',
  HomePlusElectronics = 'THE',
  HomeService = 'THS',
}

export enum PostMessageEvent {
  CloseWindow = 'CLOSE_WINDOW',
}
