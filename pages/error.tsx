import type { NextPage } from 'next';
import Head from 'next/head';
import classNames from 'classnames';
import Image from 'next/image';

import styles from '../styles/Error.module.scss';
import logo from '../public/logo.png';
import { PostMessageEvent } from '../enums';

const ErrorPage: NextPage = () => {
  const close = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    window.parent.postMessage({ action: PostMessageEvent.CloseWindow }, '*');
  };

  return (
    <>
      <Head>
        <title>Error</title>
        <meta name='description' content='My Warranty' />
        <link rel='icon' href='/favicon.ico' />
        <link
          rel='stylesheet'
          href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css'
        ></link>
      </Head>
      <div className='container text-center pt-5'>
        <Image
          alt='My Warranty'
          src={logo}
          height={75}
          width={150}
          objectFit={'contain'}
        />
        <h1>Something went wrong</h1>
        <div
          className={classNames(
            styles.error,
            'm-auto mt-4 mb-5 alert alert-danger'
          )}
        >
          <p>
            My Warranty can&apos;t find the appropriate products for you.
            Possible reasons:
          </p>
          <ul className={'mb-2 text-start m-auto'}>
            <li>Incorrect Credentials</li>
            <li>No products availabls for the specified type</li>
          </ul>
        </div>
        <button className='btn btn-primary btn-lg' onClick={close}>
          Go back
        </button>
      </div>
    </>
  );
};

export default ErrorPage;
