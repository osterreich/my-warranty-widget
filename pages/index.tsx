import type { GetServerSideProps, NextPage } from 'next';
import { useEffect } from 'react';
import Head from 'next/head';

import { useAppDispatch } from '../store';
import { productsActions } from '../store/products';
import { appActions } from '../store/app';
import { IProductBundle } from '../interfaces';
import EnrollWizard from '../components/enroll-wizard/EnrollWizard';
import MockApiService from '../services/MockApiService';

interface PropsType {
  products: IProductBundle[];
  partnerSecret: string;
  partnerSalesCode: string;
}

const Home: NextPage<PropsType> = ({
  products,
  partnerSecret,
  partnerSalesCode,
}: PropsType) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (products.length) {
      dispatch(productsActions.setSelectedProduct(products[0]));
      dispatch(productsActions.setProducts(products));
      dispatch(appActions.setPartnerSecret(partnerSecret));
      dispatch(appActions.setPartnerSalesCode(partnerSalesCode));
    }
  }, [dispatch, products, partnerSecret, partnerSalesCode]);

  return (
    <>
      <Head>
        <title>My Warranty Widget</title>
        <meta name='description' content='My Warranty' />
        <link rel='icon' href='/favicon.ico' />
        <link
          rel='stylesheet'
          href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css'
        ></link>
      </Head>
      <EnrollWizard />
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  try {
    const {
      partnerSecret = null,
      partnerSalesCode = null,
      type = 'all',
    } = query;

    const products = await MockApiService.getProducts(
      partnerSecret,
      partnerSalesCode,
      type
    );

    if (products?.length) {
      return {
        props: {
          products,
          partnerSecret,
          partnerSalesCode,
        },
      };
    } else {
      throw 'No Products available';
    }
  } catch (error) {
    console.log(error);
    return {
      redirect: {
        permanent: false,
        destination: '/error',
      },
      props: {},
    };
  }
};

export default Home;
