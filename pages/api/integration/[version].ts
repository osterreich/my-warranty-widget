import fs from 'fs';
import path from 'path';
import type { NextApiRequest, NextApiResponse } from 'next';

import { sendError } from '../../../utils';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    const { version } = req.query;
    const filePath = path.resolve(`assets/integration/script-${version}.js`);
    const readStream = fs.createReadStream(filePath);

    res.setHeader('Content-Type', 'text/javascript');
    await new Promise((resolve, reject) => {
      readStream.pipe(res);
      readStream.on('end', resolve);
      readStream.on('error', reject);
    });
  } catch (error) {
    sendError(res, error);
  }
}
