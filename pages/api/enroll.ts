import type { NextApiResponse } from 'next';

import { IEnrollRequest, IEnrollResponse } from '../../interfaces/enroll';
import { sendError } from '../../utils';
import MockApiService from '../../services/MockApiService';

export default async function handler(
  req: IEnrollRequest,
  res: NextApiResponse<IEnrollResponse | null>
) {
  try {
    const { data } = await MockApiService.enroll(req.body);

    res.status(200).json(data);
  } catch (error) {
    sendError(res, error);
  }
}
